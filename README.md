# msa-sample

study semantic versioning gitlab

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

starting branch
* `main`
* `dev/pre-alpha`

Local : 로컬 서버
- 내 PC(톰캣, lost:8080으로 띄우는 서버)

Dev : 개발 서버 
- 로컬 서버에서 개발자들이 각자 만든 코드를 합쳐서 테스트해볼 수 있는 서버
- 여러 개의 컴포넌트, 모듈을 동시에 개발할 때, 이것들을 통합 및 테스트 하는 환경으로 사용한다. Dev 환경에 배포가 되면 주기적으로 Integration 환경에 배포를 한다.

Staging :  스테이징 서버 
- Production 환경과 매우 유사하게 사용을 하고, 기능, 비 기능 테스트 모두 수행된다.
- Production 환경과 거의 동일하게 운영을 하며 여러가지 보안이나 성능 측면들을 검증할 때 활용한다.

Product(PROD) : 운영 서버 
- 실제 서비스를 위한 운영 서버

## Setup Repo setting

Branch name in Push rules setting

```
(^dev/|^revert-|^cherry-pick-)
```

Protected branches

* `main` Allowed to merge : `Maintainers`, Allowed to push : `No one`, Allowed to force push : `false`, Code owner approval : `false`

Protected tags

* `*` Allowed to create `No one`

## branching model

1. dev branche를 사용하고 master에서 직접 커밋하지 않음
2. master의 커밋뿐만 아니라 모든 커밋을 테스트합니다.
3. 모든 커밋에 대해 모든 테스트를 실행합니다.
   (테스트가 5분 이상 실행되는 경우 병렬로 실행).
4. 이후가 아니라 마스터에 병합하기 전에 코드 검토를 수행합니다.
5. 배포는 마스터에서만 합니다. CI 기반으로 합니다.
6. 태그는 사용하지 않습니다.
7. 릴리스하지 않습니다.
8. 푸시된 커밋은 리베이스되지 않습니다.
9. 모든 사람은 마스터에서 시작하여 마스터를 대상으로 합니다.
10. 커밋 메시지에는 의도가 반영됩니다.
머지 리퀘스트 할때 Shamsh로 합니다.

## tutorial

Project 리더가 배정됩니다. 프로젝트의 기획서를 분석하고 마일드 스톤, 기술 스택, 브랜칭 전략 선택, 언어, 프레임워크를 선정합니다.

이 튜토리얼은 MSA에 적합한 브랜칭 전략.

### step1. pre-alpha

배포 설정을 합니다.

* `dev/pre-alpha` 브런치 생성 및 체크 아웃합니다.
* root에 `edit-ci.txt` file 추가.
* `edit-ci.txt`파일에 변경내용 `ci: make deploy-env setting` 추가 후 commit
* 원격에 푸쉬합니다.
* `main`에게 `dev/pre-alpha`를 MR 합니다.

`env/dev` 배포합니다.

* `env/dev` CI를 실행합니다.

`env/dev` 배포에 실패합니다. `env/dev` 배포를 수정합니다.

* `main`을 Base로 `dev/fix-env-dev` 브런치 생성 및 체크 아웃합니다.
* `edit-ci.txt`파일에 변경내용 `ci: env-dev setting` 추가 후 commit
* 원격에 푸쉬합니다.
* `main`에게 `dev/fix-env-dev`를 MR 합니다.

`env/dev` 배포합니다.

* `env/dev` CI를 실행합니다. 

`env/dev` 배포에 성공합니다. `env/staging` 배포합니다.

* `env/staging` CI를 실행합니다. 

`env/staging` 배포 결과는 실패로 가정합니다. `env/staging` 배포를 수정합니다.

* `main`을 Base로 `dev/fix-env-staging` 브런치 생성 및 체크 아웃합니다.
* `edit-ci.txt`파일에 변경내용 `ci: env-staging setting` 추가 후 commit
* 원격에 푸쉬합니다.
* `main`에게 `dev/fix-env-staging`를 MR 합니다.

`env/dev` 배포합니다.

* `env/dev` CI를 실행합니다. 

`env/dev` 배포에 성공합니다. `env/staging` 배포합니다.

* `env/staging` CI를 실행합니다. 

`env/staging` 배포에 성공합니다.

이제 시작 프로젝트 준비가 끝났습니다. DevOps Team이 구성됩니다.

### step3. 기능 구현

개발자 리더는 요구사항에 맞춰서 A기능과 B기능을 개발하기로 계획합니다.
마일드 스톤 등록... 관리자 할당
이슈 등록... 담당 개발자 할당

`@개발자`는 A기능을 개발 합니다.

* `main`을 Base로 `dev/feature-a` 브런치 생성 및 체크 아웃합니다.
* root에 `featrue-a.txt` file 추가.
* `featrue-a.txt`파일에 변경내용 `feat: develop a` 추가 후 commit
* 원격에 푸쉬합니다.

개발자는 B기능을 개발 합니다.

* `main`을 Base로 `dev/feature-b` 브런치 생성 및 체크 아웃합니다.
* root에 `featrue-b.txt` file 추가.
* `featrue-b.txt`파일에 변경내용 `feat: develop b` 추가 후 commit
* 원격에 푸쉬합니다.
* `main`에게 `dev/feature-a`를 MR 합니다.

운영 팀장은 `env/dev`으로 배포 합니다.

* `env/dev` CI를 실행합니다.

운영자는 `env/dev` 환경에서 A기능이 문제를 발견합니다.

* `main`을 Base로 `dev/fix-a` 브런치 생성 및 체크 아웃합니다.
* `featrue-a.txt`파일에 변경내용 `fix: develop a` 추가 후 commit
* 원격에 푸쉬합니다.

C기능을 개발 합니다.

* `main`을 Base로 `dev/feature-c` 브런치 생성 및 체크 아웃합니다.
* root에 `featrue-c.txt` file 추가.
* `featrue-c.txt`파일에 변경내용 `feat: develop c` 추가 후 commit
* 원격에 푸쉬합니다.
* `main`에게 `dev/fix-a`를 MR 합니다.
* `main`에게 `dev/feature-b`를 MR 합니다.

ops team은 프로그램을 `env/dev`으로 배포 합니다.

* `env/dev` CI를 실행합니다. 

`env/dev` 배포에 성공합니다. 프로그램 테스트 결과 발견된 issue가 없어서 `env/staging`으로 배포 합니다.

* `env/staging` CI를 실행합니다. 

`env/staging` 배포에 성공합니다. 프로그램 테스트 결과 A기능과 B기능의 issue를 발견합니다.

* developer는 `main`을 Base로 `dev/fix-b` 브런치 생성 및 체크 아웃합니다.
* developer는 `featrue-b.txt`파일에 변경내용 `fix: develop b` 추가 후 commit
* developer는 원격에 푸쉬합니다.
* developer는 `main`을 Base로 `dev/fix-a` 브런치 생성 및 체크 아웃합니다.
* developer는 `featrue-a.txt`파일에 변경내용 `fix: edit a` 추가 후 commit
* developer는 원격에 푸쉬합니다.
* developer는 `main` branch로 `dev/fix-a` branch를 MR(!6) 합니다.
* dev team 리더는 MR(!6)를 승인 합니다.
* developer는 `main` branch로 `dev/fix-b` branch를 MR(!7) 합니다.
* dev team 리더는 MR(!7)를 승인 합니다.
* developer는 `main` branch로 `dev/feature-c` branch를 MR(!8) 합니다.
* dev team 리더는 MR(!8)를 승인 합니다.

ops team 리더는 `env/dev`으로 배포 합니다.

* `env/dev` CI를 실행합니다. 

`env/dev` 배포에 성공합니다. `env/staging`으로 배포 합니다.

* `env/staging` CI를 실행합니다. 

`env/staging` 배포에 성공합니다.

### step4. 제품 출시

project 총괄 리더는 `env/production`으로 배포 합니다.

* `env/production` CI를 실행합니다. 

### step5. Rollback

C기능을 개선합니다.

* `main`을 Base로 `dev/refactor-c` 브런치 생성 및 체크 아웃합니다.
* `featrue-c.txt`파일에 변경내용 `refactor: C` 추가 후 commit
* 원격에 푸쉬합니다. 
* `main`에게 `dev/refactor-c`를 MR 합니다.

D기능을 개발합니다.

* `main`을 Base로 `dev/featrue-D` 브런치 생성 및 체크 아웃합니다.
* root에 `featrue-d.txt` file 추가.
* `featrue-d.txt`파일에 변경내용 `feat: D` 추가 후 commit
* 원격에 푸쉬합니다. 
* `main`에게 `dev/featrue-D`를 MR 합니다.

C기능에서 운영을 지속하기에는 치명적인 Issue가 발견됩니다.

C기능을 제거후 C기능을 보완하기로 결정합니다.

* `main`에게 `dev/refactor-c`를 MR 했던것을 revert 합니다.
* `main`에게 `dev/feature-c`를 MR 했던것을 revert 합니다.
* `main`에게 `dev/feature-c`를 MR 했던것을 cherry-pick 해서 새로운 branch를 만듭니다. `cherry-pick-xxxx` 라고 가정합니다.
* `main`에게 `dev/refactor-c`를 MR 했던것을 cherry-pick 해서 `cherry-pick-xxxx`에 merge 시킵니다.

`cherry-pick-xxxx` branch에서 C기능을 보완합니다.

* `featrue-c.txt`파일에 변경내용 `fix: verry verry big issue` 추가
* 원격에 푸쉬합니다. 
* `main`에게 `cherry-pick-xxxx`를 MR 합니다.

## ref

https://blog.banksalad.com/tech/become-an-organization-that-deploys-1000-times-a-day/

https://tech.kakao.com/2021/07/16/devops-for-msa/